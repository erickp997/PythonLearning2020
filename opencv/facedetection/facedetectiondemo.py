from cv2 import cv2

face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

image = cv2.imread("photo.jpg")
gray_image = cv2.cvtColor(image, cv2.COLOR_RGBA2GRAY)

faces = face_cascade.detectMultiScale(
    gray_image,
    scaleFactor = 1.05,
    minNeighbors = 5
)

for x, y, width, height in faces:
    image = cv2.rectangle(image, (x, y), (x + width, y + height), (0, 255, 0), 3)

print(type(faces))
print(faces)

cv2.imshow("Gray", image)
cv2.waitKey(0)
cv2.destroyAllWindows()

news_image = cv2.imread("news.jpg")
gray_news_image = cv2.cvtColor(news_image, cv2.COLOR_RGBA2GRAY)

news_faces = face_cascade.detectMultiScale(
    gray_news_image,
    scaleFactor = 1.01,
    minNeighbors = 5
)

for x, y, width, height in news_faces:
    news_image = cv2.rectangle(news_image, (x, y), (x + width, y + height), (0, 255, 0), 3)

print(type(news_faces))
print(news_faces)

news_image = cv2.resize(news_image, (int(news_image.shape[1]/3),(int(news_image.shape[0]/3))))
cv2.imshow("Gray", news_image)
cv2.waitKey(0)
cv2.destroyAllWindows()