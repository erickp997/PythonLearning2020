from cv2 import cv2
import glob

images = glob.glob("*.jpg")

for image in images:
    sample_image = cv2.imread(image, 0)
    resized_sample_image = cv2.resize(sample_image, (100, 100))
    cv2.imshow("Test", resized_sample_image)
    cv2.waitKey(500)
    cv2.destroyAllWindows()
    cv2.imwrite("resized_" + image, resized_sample_image)