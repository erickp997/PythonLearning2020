from cv2 import cv2
import time

video = cv2.VideoCapture(0)
i = 0

while True:
    i = i + 1
    check, frame = video.read()

    print(check)
    print(frame)

    gray_video = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("Capturing", gray_video)

    key = cv2.waitKey(1)
    
    if key == ord('q'):
        break

print(i)
video.release()
cv2.destroyAllWindows