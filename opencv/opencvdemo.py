from cv2 import cv2

image = cv2.imread("galaxy.jpg", 0)

print(type(image))

print(image)

'''
Output: 
<class 'numpy.ndarray'>
[[14 18 14 ... 20 15 16]
 [12 16 12 ... 20 15 17]
 [12 13 16 ... 14 24 21]
 ...
 [ 0  0  0 ...  5  8 14]
 [ 0  0  0 ...  2  3  9]
 [ 1  1  1 ...  1  1  3]]
'''

print(image.shape)

print(image.ndim)

'''
Output:
(1485, 990)
2
'''

cv2.imshow("Galaxy", image)
cv2.waitKey(2000)
cv2.destroyAllWindows()

'''
Output:
(external window with the image that closes after 2000 ms or 2 seconds)
'''

resized_image = cv2.resize(image, (int(image.shape[1]/2), int(image.shape[0]/2)))
cv2.imwrite("Galaxy_resized.jpg", resized_image)
cv2.imshow("Galaxy_resized", resized_image)
cv2.waitKey(2000)
cv2.destroyAllWindows()
'''
Output:
(creates a new image in the working directory and closes after 2000 ms)
'''