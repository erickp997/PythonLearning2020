import folium
import pandas

'''
Read the Volcano data in which contains
VOLCANX020,NUMBER,NAME,LOCATION,STATUS,ELEV,TYPE,TIMEFRAME,LAT,LON
'''
volcano_data = pandas.read_csv("Volcanoes.txt")
latitude = list(volcano_data["LAT"])
longitude = list(volcano_data["LON"])
elevation = list(volcano_data["ELEV"])
volcano_name = list(volcano_data["NAME"])

# Map object initiation values
latitude_start = 38.58
longitude_start = -99.09
zoom_level_start = 5
map_tile = "Stamen Terrain"
one_km = 1000
three_km = 3000
world_data_file = 'world.json'
world_data_file_mode = 'r'
world_data_file_encoding = 'utf-8-sig'
ten_million = 10000000
twenty_million = 20000000
small_population = 'green'
medium_population = 'orange'
large_population = 'red'

# Popup iFrame basic html styling
popup_html = """
<h4>Volcano information:</h4><br>
<a href="https://www.google.com/search?q=%%22%s%%22" target="_blank">%s</a><br>
Height: %s m
"""
iframe_width = 200
iframe_height = 100

# Add colors based on elevation of volcanos
def elevation_color_picker(elevation_value):
    if elevation_value < one_km:
        return 'green'
    elif one_km <= elevation_value < three_km:
        return 'orange'
    else:
        return 'red'

# For simplicity pass in just the location, zoom_start, and tiles parameters 
# rather than all of the different ones
map = folium.Map(
    location = [latitude_start, longitude_start],
    zoom_start = zoom_level_start,
    tiles = map_tile
)

# Initialize the volcano feature group for map object
volcanoes_feature_group_name = "Volcanoes"
volcano_feature_group = folium.FeatureGroup(
    name = volcanoes_feature_group_name
)

# x, y, and z are essentially latitude, longitude, and elevation
for x, y, z, name in zip(latitude, longitude, elevation, volcano_name):
    iframe = folium.IFrame(
        html = popup_html % (name, name, z),
        width = iframe_width,
        height = iframe_height
    )
    volcano_feature_group.add_child(
        folium.Marker(
            location = [x, y], 
            popup = folium.Popup(iframe),
            icon = folium.Icon(
                color = elevation_color_picker(z)
            )
        )
    )

# Initialize the population feature group for map object
population_feature_group_name = "Population"
population_feature_group = folium.FeatureGroup(
    name = population_feature_group_name
)

population_feature_group.add_child(
    folium.GeoJson(
        data = open(
            world_data_file, 
            world_data_file_mode, 
            encoding = world_data_file_encoding
        ).read(),
        style_function = lambda x : {
            'fillColor': small_population if x['properties']['POP2005'] < ten_million
            else medium_population if ten_million <= x['properties']['POP2005'] < twenty_million else large_population
        }
    )
)

# Create the feature groups on the map
map.add_child(volcano_feature_group)
map.add_child(population_feature_group)
map.add_child(folium.LayerControl())

# Save the map and then open the created file in preferred browser
map.save("webmap.html")