import requests, pandas
from bs4 import BeautifulSoup

site = 'http://pyclass.com/real-estate/rock-springs-wy/LCWYROCKSPRINGS/'

siterequest = requests.get( 
    site, 
    headers = 
    {
        'User-agent' : 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'
    }
)

content = siterequest.content

soup = BeautifulSoup(
    content, 
    "html.parser" 
)

all = soup.find_all( 
    "div", 
    { 
        "class" : "propertyRow" 
    } 
)


for item in all:
    datastorage = {}
    datastorage[
        "Price"
    ] = item.find( 
        "h4", 
        { 
            "class" : "propPrice" 
        }
    ).text.replace(
        "\n", 
        ""
    ).replace(
        " ", 
        ""
    )

    datastorage[
        "Property Address"
    ] = item.find_all(
        "span",
        {
            "class" : "propAddressCollapse"
        }
    )[ 
        0 
    ].text

    datastorage[
        "Property City"
    ] = item.find_all(
            "span",
            {
                "class" : "propAddressCollapse"
            }
    )[
        1 
    ].text

    try:
        datastorage[
            "Property Bedrooms"
        ] = item.find(
            "span",
            {
                "class" : "infoBed"
            }
        ).find(
            "b"
        ).text
    except:
        datastorage[
            "Property Bedrooms"
        ] = None

    try:
        datastorage[
            "Property Square Feet"
        ] = item.find(
                "span",
                {
                    "class" : "infoSqFt"
                }
            ).find(
                "b"
            ).text
    except:
        datastorage[
            "Property Square Feet"
        ] = None

    try:
        datastorage[
            "Property Full Bathrooms"
        ] = item.find(
                "span",
                {
                    "class" : "infoValueFullBath"
                }
            ).find(
                "b"
            ).text
    except:
        datastorage[
            "Property Full Bathrooms"
        ] = None
    
    try:
        datastorage[
            "Property Half Bathrooms"
        ] = item.find(
                "span",
                {
                    "class" : "infoValueHalfBath"
                }
            ).find(
                "b"
            ).text
    except:
        datastorage[
            "Property Half Bathrooms"
        ] = None

    for column_group in item.find_all(
        "div",
        {
            "class" : "columnGroup"
        }
    ):
        for feature_group, feature_name in zip(
            column_group.find_all(
                "span",
                {
                    "class" : "featureGroup"
                }
            ),
            column_group.find_all(
                "span",
                {
                    "class" : "featureName"
                }
            )
        ):
            if "Lot Size" in feature_group.text:
                datastorage[
                    "Lot Size"
                ] = feature_name.text


listofdictionaries.append(datastorage)

dataframe = pandas.DataFrame(listofdictionaries)

dataframe.to_csv( "PropertyData.csv" )