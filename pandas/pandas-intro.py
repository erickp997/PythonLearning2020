import pandas

# One way to display dataframes with pandas

df1 = pandas.DataFrame([[2,4,6],[10,20,30]])

print(df1)

df1 = pandas.DataFrame(
    [[2,4,6],[10,20,30]], 
    columns=["Price", "Age", "Value"]
)

print(df1)

df1 = pandas.DataFrame(
    [[2,4,6],[10,20,30]], 
    columns=["Price","Age","Value"], 
    index=["First","Second"]
)

print(df1)

# Get the mean of the data
print("\nThe mean of the data is")
print(df1.mean())

print("\nThe mean of the mean is")
print(df1.mean().mean())

# Get a specific series of data
print("\nGet the info on the Price column only")
print(df1.Price)

print("\nGet the mean of the Price column only")
print(df1.Price.mean())

print("\nGet the max of the Price column only")
print(df1.Price.max())

# Alternate way to display dataframes with pandas

df2 = pandas.DataFrame(
    [
        {"Name":"John", "Surname":"Johns"},
        {"Name":"Jack"}
    ]
)

print("\n Alternate way to display dataframes with pandas")
print(df2)


# Get all of the methods that can be used with dataframes
## print(dir(df1))