import os
import pandas
from geopy.geocoders import Nominatim

print(os.listdir())

# loading with csv
dataframe1 = pandas.read_csv("supermarkets.csv")
print(dataframe1)

# loading with json file
dataframe2 = pandas.read_json("supermarkets.json")
print(dataframe2)

# loading with excel file
dataframe3 = pandas.read_excel("supermarkets.xlsx", sheet_name=0)
print(dataframe3)

# loading with txt file with commas -- sep',' is default value, does not need to be stated explicitly when comma delimited
dataframe4 = pandas.read_csv("supermarkets-commas.txt", sep=',')
print(dataframe4)

# loading with txt file with semicolon
dataframe5 = pandas.read_csv("supermarkets-semi-colons.txt", sep=';')
print(dataframe5)

# loading data from a text file with no header
dataframe6 = pandas.read_csv("data.txt", header = None)
print(dataframe6)

# giving the dataframe a header
dataframe6.columns = ["ID","Address","City","ZIP","Country","Name","Employees"]
print(dataframe6)

dataframe6.set_index("ID")
print(dataframe6)

# Splice data by Index and Range
dataframe7 = pandas.read_csv("supermarkets.csv")
dataframe7 = dataframe7.set_index("Address")
print(dataframe7.loc["735 Dolores St":"3995 23rd St","ID":"Country"])

# Print country column as a list
print(list(dataframe7.loc[:,"Country"]))

# Print as position based indexing
print(dataframe7.iloc[3,1:4])

# Remove an entry
dataframe7 = dataframe7.drop("332 Hill St", 0)
print(dataframe7)

# Drop based on rows
# dataframe7 = dataframe7.drop(dataframe7.index[0:3], 0)

# Drop based on columns
dataframe7 = dataframe7.drop(dataframe7.columns[0:3], 1)
print(dataframe7)

# Add a column of shape[0] rows or 5 rows which is the number of rows in the table
dataframe7["Continent"] = dataframe7.shape[0]*["North America"]

# In the new column,  contatenate the entries of column "Country" with a comma and the continent entries
dataframe7["Continent"] = dataframe7["Country"] + " , " + "North America"
print(dataframe7)

# Transpose data
'''
dataframe7_transposed = dataframe7.T
print(dataframe7_transposed)

dataframe7_transposed["My Address"] = ["My City", "My Country", 10, 7, "My Shop", "My State", "My Continent"]
print(dataframe7_transposed)
'''

# Create location variable using Nominatim object
nom = Nominatim()
location = nom.geocode("3995 23rd St, San Francisco, CA 94114")

print(location)
print(location.longitude)

market_dataframe = pandas.read_csv("supermarkets.csv")
market_dataframe["Address"] = market_dataframe["Address"] + ", " + market_dataframe["City"] + ", " + market_dataframe["State"] + ", " + market_dataframe["Country"]

# Append coordinates to dataframe table
market_dataframe["Coordinates"] = market_dataframe["Address"].apply(nom.geocode)
print(market_dataframe)

# Append Latitude and Longitude to dataframe table
market_dataframe["Latitude"] = market_dataframe["Coordinates"].apply(lambda x: x.latitude if x != None else None)
market_dataframe["Longitude"] = market_dataframe["Coordinates"].apply(lambda x: x.longitude if x!= None else None)
print(market_dataframe)