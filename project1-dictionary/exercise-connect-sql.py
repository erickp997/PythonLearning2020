import mysql.connector

dictionary_connector = mysql.connector.connect(
    user = "{user_redacted}",
    password = "{password_redacted}",
    host = "{host_redacted}",
    database = "{database_redacted}"
)

cursor = dictionary_connector.cursor()
word = input("Enter a word: ")

## Find an exact word match
query = cursor.execute("SELECT * FROM Dictionary WHERE Expression = '%s' " % word)
'''
## Find all the rows where the value of the column expression starts with the letter r
query = cursor.execute("SELECT * FROM Dictionary WHERE Expression LIKE '%r' ")

## Find all the rows where the value of the column expression starts with the word rain
query = cursor.execute("SELECT * FROM Dictionary WHERE Expression LIKE '%rain' ")

## Find all the rows where the length of the value of the column Expression is less than four characters
query = cursor.execute("SELECT * FROM Dictionary WHERE length(Expression) < 4 ")

## Find all the rows where the length of the value of the column Expression is greater than 1 but less than 4 characters
query = cursor.execute("SELECT * FROM Dictionary WHERE length(Expression) > 1 AND length(Expression) < 4 ")

## Find all the rows of column Definition where the value of the column Expression starts with r
query = cursor.execute("SELECT Definition FROM Dictionary WHERE Expression  LIKE 'r%' ")
'''

results = cursor.fetchall()

if results:
    for result in results:
        print(result[1])
else:
    print("No word found")