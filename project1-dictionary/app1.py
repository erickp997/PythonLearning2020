import json
from difflib import get_close_matches

data = json.load(open("data.json"))

def translate(word):
    word = word.lower()
    if word in data:
        return data[word]
    elif word.title() in data:
        return data[word.title()]
    elif word.upper() in data:
        return data[word.upper()]
    elif len(get_close_matches(word, data.keys())) > 0:
        user_confirmation = input("Did you mean %s isntead? Enter Y if yes or N if no: " % get_close_matches(word, data.keys())[0])
        if user_confirmation == "Y":
            return data[get_close_matches(word, data.keys())[0]]
        elif user_confirmation == "N":
            return "The word doesn't exist. Please double check your input."
        else:
            return "We don't understand what you're trying to input."
    else:
        return "The word doesn't exist. Please double check your input."

word_input = input("Enter a word: ")

output = translate(word_input)

if type(output) == list:
    for i in output:
        print(i)
else:
    print(output)