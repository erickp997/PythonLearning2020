import numpy
import cv2

# Open an image and read it in grayscale
image_gray = cv2.imread("smallgray.png", 0)
print(image_gray)

'''
Open an image and read it in as RGB (3 dimensional array):

[[[pixel1_red, pixel2_red, ...]
...
[pixeln-2_red, pixeln-1_red, pixeln_red]]

[[pixel1_green, pixel2_green, ...]
...
[pixeln-2_green, pixeln-1_green, pixeln_green]]

[[pixel1_blue, pixel2_blue, ...]
...
[pixeln-2_blue, pixeln-1_blue, pixeln_blue]]]
'''
image_rgb = cv2.imread("smallgray.png", 1)
print(image_rgb)

# Create an image with a numpy array
cv2.imwrite("newsmallgray.png", image_gray)

# Slice numpy array (elements at first 2 rows and 3 and 4th columns)
print(image_gray)
print(image_gray[0:2, 2:4])
print(image_gray[0:2, 2:4].shape)

# Get value at index of row 2 and column 4 -- counting starts at 0!
print(image_gray)
print(image_gray[2,4])

# Print just the rows
for i in image_gray:
    print(i)

# Print the rows transposed
for i in image_gray.T:
    print(i)

# Print just the values, not rows, columns, lists, arrays etc...
for i in image_gray.flat:
    print(i)

# Stacking horizontally numpy arrays, pass 1 tuple to work around the 1 
# parameter requirement for stacking n arrays
image_stack = numpy.hstack((image_gray,image_gray))
print(image_stack)

# Stacking vertically numpy arrays, pass 1 tuple to work around the 1 parameter 
# requirement for stacking n arrays
image_stack = numpy.vstack((image_gray,image_gray))
print(image_stack)


# Splitting horizontally numpy arrays (5 arrays)
horizontal_numpy_split = numpy.hsplit(image_stack, 5)
print(horizontal_numpy_split)

# Splitting vertically numpy arrays (3 arrays)
vertically_numpy_split = numpy.vsplit(image_stack, 3)
print(vertically_numpy_split)