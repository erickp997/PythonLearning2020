import numpy

# Create a numpy array with the first 27 digits including 0
n = numpy.arange(27)

print(n)

# Create a 2 dimensional array of 3 rows and 9 columns
print(n.reshape(3, 9))

# Create a 3 dimensional array of 3 rows, 3 columns, and 3 layers
print(n.reshape(3, 3, 3))

# Convert a Python list to a numpy array for more efficient functionality
python_list = [[123, 12, 123, 12, 33], [], []]
python_list_to_numpy_array = numpy.asarray(python_list)

print(python_list_to_numpy_array)